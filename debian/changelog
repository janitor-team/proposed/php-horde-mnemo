php-horde-mnemo (4.2.14-10) unstable; urgency=medium

  * d/patches: Regression fix in 1010_phpunit-8.x+9.x.patch.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 21 Oct 2020 17:50:51 +0200

php-horde-mnemo (4.2.14-9) unstable; urgency=medium

  * d/patches: Ignore KolabTest.php for given reason.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 20 Oct 2020 14:47:44 +0000

php-horde-mnemo (4.2.14-8) unstable; urgency=medium

  * Currently, all unit tests are ignored by upstream.

  * d/patches: Add 1010_phpunit-8.x+9.x.patch. Fix tests with PHPUnit 8.x/9.x.
  * d/t/control: Require php-horde-test (>= 2.6.4+debian0-6~).

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 03 Oct 2020 17:55:16 +0200

php-horde-mnemo (4.2.14-7) unstable; urgency=medium

  * d/control: Enforce versioned D on php-horde (>= 5.2.23+debian0-1~).
  * d/rules: Move theme files to /etc/horde/.
  * d/maintscript: Turn /usr/share/horde/mnemo/themes/ into symlink.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 03 Jul 2020 23:50:28 +0200

php-horde-mnemo (4.2.14-6) unstable; urgency=medium

  [ Mike Gabriel ]
  * d/control: Add to Uploaders: Juri Grabowski.
  * d/control: Bump DH compat level to version 13.
  * d/salsa-ci.yml: Add file with salsa-ci.yml and pipeline-jobs.yml calls.
  * d/copyright: Fix typo in path name.

  [ Juri Grabowski ]
  * d/salsa-ci.yml: enable aptly
  * d/salsa-ci.yml: allow_failure on autopkgtest.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 01 Jul 2020 12:47:39 +0200

php-horde-mnemo (4.2.14-5) unstable; urgency=medium

  * Re-upload to Debian. (Closes: #959321).

  * d/control: Add to Uploaders: Mike Gabriel.
  * d/control: Drop from Uploaders: Debian QA Group.
  * d/control: Bump Standards-Version: to 4.5.0. No changes needed.
  * d/control: Add Rules-Requires-Root: field and set it to 'no'.
  * d/upstream/metadata: Add file. Comply with DEP-12.
  * d/tests/control: Stop using deprecated needs-recommends restriction.
  * d/copyright: Update copyright attributions.
  * d/lintian-overrides: Override description-synopsis-starts-with-article.
  * d/rules: Don't install upstream's INSTALL howto.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 13 May 2020 21:16:38 +0200

php-horde-mnemo (4.2.14-4) unstable; urgency=medium

  * Trim trailing whitespace.
  * Bump debhelper from old 11 to 12.
  * d/control: Orphaning package (See #942282)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 18 Oct 2019 20:41:52 +0200

php-horde-mnemo (4.2.14-3) unstable; urgency=medium

  * Update Standards-Version to 4.1.4, no change
  * Update Maintainer field

 -- Mathieu Parent <sathieu@debian.org>  Tue, 15 May 2018 16:21:48 +0200

php-horde-mnemo (4.2.14-2) unstable; urgency=medium

  * Update Standards-Version to 4.1.3, no change
  * Upgrade debhelper to compat 11
  * Update Vcs-* fields
  * Use secure copyright format URI
  * Replace "Priority: extra" by "Priority: optional"

 -- Mathieu Parent <sathieu@debian.org>  Fri, 06 Apr 2018 13:10:03 +0200

php-horde-mnemo (4.2.14-1) unstable; urgency=medium

  * New upstream version 4.2.14

 -- Mathieu Parent <sathieu@debian.org>  Wed, 27 Sep 2017 21:34:46 +0200

php-horde-mnemo (4.2.13-1) unstable; urgency=medium

  * New upstream version 4.2.13

 -- Mathieu Parent <sathieu@debian.org>  Sat, 01 Jul 2017 22:25:41 +0200

php-horde-mnemo (4.2.12-1) unstable; urgency=medium

  * New upstream version 4.2.12

 -- Mathieu Parent <sathieu@debian.org>  Sun, 18 Dec 2016 22:53:50 +0100

php-horde-mnemo (4.2.11-1) unstable; urgency=medium

  * New upstream version 4.2.11

 -- Mathieu Parent <sathieu@debian.org>  Thu, 07 Jul 2016 21:32:48 +0200

php-horde-mnemo (4.2.10-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.8, no change
  * Updated d/watch to use https

 -- Mathieu Parent <sathieu@debian.org>  Wed, 08 Jun 2016 21:13:57 +0200

php-horde-mnemo (4.2.10-1) unstable; urgency=medium

  * New upstream version 4.2.10

 -- Mathieu Parent <sathieu@debian.org>  Wed, 06 Apr 2016 08:15:30 +0200

php-horde-mnemo (4.2.9-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.7, no change
  * Use secure Vcs-* fields
  * Rebuild with newer pkg-php-tools for the PHP 7 transition
  * Replace php5-* by php-* in d/tests/control

 -- Mathieu Parent <sathieu@debian.org>  Sun, 13 Mar 2016 20:55:33 +0100

php-horde-mnemo (4.2.9-1) unstable; urgency=medium

  * New upstream version 4.2.9

 -- Mathieu Parent <sathieu@debian.org>  Sat, 06 Feb 2016 22:14:07 +0100

php-horde-mnemo (4.2.8-1) unstable; urgency=medium

  * Upgaded to debhelper compat 9
  * New upstream version 4.2.8

 -- Mathieu Parent <sathieu@debian.org>  Sat, 24 Oct 2015 07:40:12 +0200

php-horde-mnemo (4.2.7-1) unstable; urgency=medium

  * New upstream version 4.2.7

 -- Mathieu Parent <sathieu@debian.org>  Mon, 17 Aug 2015 20:04:05 +0200

php-horde-mnemo (4.2.6-2) unstable; urgency=medium

  * Remove XS-Testsuite header in d/control
  * Update gbp.conf
  * Use override_dh_link instead of dh deprecated --until/--after

 -- Mathieu Parent <sathieu@debian.org>  Mon, 10 Aug 2015 01:22:20 +0200

php-horde-mnemo (4.2.6-1) unstable; urgency=medium

  * Update Standards-Version to 3.9.6, no change
  * New upstream version 4.2.6
    - Patch for #772474 included, dropped

 -- Mathieu Parent <sathieu@debian.org>  Tue, 05 May 2015 08:55:36 +0200

php-horde-mnemo (4.2.1-5) unstable; urgency=medium

  * Check permission when editing notes (Closes: #772474)

 -- Mathieu Parent <sathieu@debian.org>  Sun, 07 Dec 2014 16:41:39 +0100

php-horde-mnemo (4.2.1-4) unstable; urgency=medium

  * Fixed DEP-8 tests, by removing "set -x"

 -- Mathieu Parent <sathieu@debian.org>  Sat, 11 Oct 2014 15:01:35 +0200

php-horde-mnemo (4.2.1-3) unstable; urgency=medium

  * Fixed DEP-8 tests

 -- Mathieu Parent <sathieu@debian.org>  Sat, 13 Sep 2014 15:35:30 +0200

php-horde-mnemo (4.2.1-2) unstable; urgency=medium

  * Update Standards-Version, no change
  * Update Vcs-Browser to use cgit instead of gitweb
  * Add dep-8 (automatic as-installed package testing)

 -- Mathieu Parent <sathieu@debian.org>  Wed, 27 Aug 2014 07:38:29 +0200

php-horde-mnemo (4.2.1-1) unstable; urgency=medium

  * New upstream version 4.2.1

 -- Mathieu Parent <sathieu@debian.org>  Tue, 05 Aug 2014 12:03:00 +0200

php-horde-mnemo (4.2.0-1) unstable; urgency=medium

  * New upstream version 4.2.0

 -- Mathieu Parent <sathieu@debian.org>  Wed, 16 Jul 2014 00:06:44 +0200

php-horde-mnemo (4.2.0~alpha1-1) unstable; urgency=medium

  * New upstream version 4.2.0~alpha1

 -- Mathieu Parent <sathieu@debian.org>  Mon, 09 Jun 2014 10:27:56 +0200

php-horde-mnemo (4.1.3-1) unstable; urgency=medium

  * New upstream version 4.1.3

 -- Mathieu Parent <sathieu@debian.org>  Tue, 11 Mar 2014 21:26:20 +0100

php-horde-mnemo (4.1.2-1) unstable; urgency=low

  * New upstream version 4.1.2

 -- Mathieu Parent <sathieu@debian.org>  Tue, 29 Oct 2013 21:57:46 +0100

php-horde-mnemo (4.1.1-1) unstable; urgency=low

  * New upstream version 4.1.1

 -- Mathieu Parent <sathieu@debian.org>  Sun, 11 Aug 2013 13:11:42 +0200

php-horde-mnemo (4.1.0-1) unstable; urgency=low

  * New upstream version 4.1.0
  * Use pristine-tar

 -- Mathieu Parent <sathieu@debian.org>  Wed, 12 Jun 2013 21:29:05 +0200

php-horde-mnemo (4.0.3-1) unstable; urgency=low

  * New upstream version 4.0.3

 -- Mathieu Parent <sathieu@debian.org>  Sun, 07 Apr 2013 17:48:24 +0200

php-horde-mnemo (4.0.2-1) unstable; urgency=low

  * New upstream version 4.0.2

 -- Mathieu Parent <sathieu@debian.org>  Thu, 10 Jan 2013 22:43:06 +0100

php-horde-mnemo (4.0.1-2) unstable; urgency=low

  * Add a description of Horde in long description
  * Updated Standards-Version to 3.9.4, no changes
  * Replace horde4 by PEAR in git reporitory path
  * Fix Horde Homepage
  * Fix FTBFS due to rm failure with older pkg-php-tools (Closes: #697773).
    Thanks to Chris J Arges.
  * Remove debian/pearrc, not needed with latest php-horde-role

 -- Mathieu Parent <sathieu@debian.org>  Wed, 09 Jan 2013 21:09:50 +0100

php-horde-mnemo (4.0.1-1) unstable; urgency=low

  * mnemo package
  * Initial packaging (Closes: #657381)
  * Copyright file by Soren Stoutner and Jay Barksdale

 -- Mathieu Parent <sathieu@debian.org>  Sat, 01 Dec 2012 11:53:48 +0100
